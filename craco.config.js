const CracoLessPlugin = require("craco-less");
const CracoAlias = require("craco-alias");

module.exports = {
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: "options",
        baseUrl: "./",
        aliases: {
          "@core": "./src/core",
          "@features": "./src/features",
          "@pages": "./src/pages",
          "@components": "./src/components",
          "@assets": "./src/assets",
          "@const": "./src/const",
        },
      },
    },
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@primary-color": "#0d656d",
              "@btn-primary-color": "#0d656d",
              "@border-radius-base": "6px",
              "@btn-border-radius-base": "12px",
              // "@btn-padding-horizontal-base": "5rem",
              // "@btn-padding-vertical-base": "2rem",
              "@input-padding-horizontal-base": "1rem",
              "@input-padding-vertical-base": "0.8rem",
              "@font-size-base": "14px",
              "@btn-default-color": "#fbfbfb",
              "@select-border-color": "rgb(90, 146, 153)",
              "@label-color": "rgba(0, 0, 0, 0.85)",
              // "@text-color": "#285158",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
