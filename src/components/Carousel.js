import { Carousel } from "antd";
import React from "react";
import "@assets/scss/login.scss";
import pic1 from "@assets/img/pic1.jpg";
import pic2 from "@assets/img/pic2.jpg";
import pic3 from "@assets/img/pic3.jpg";


const CarouselLogin = () => {
  return (
    <Carousel autoplay>
      <img src={pic1} alt="pic1"></img>
      <img src={pic2} alt="pic2"></img>
      <img src={pic3} alt="pic3"></img>
    </Carousel>
  );
};

export default CarouselLogin;
