import React, { useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";

// import HeaderLayout from "@components/Header";
// import FooterLayout from "@components/Footer";
import "./App.less";
import { publicRouter } from "@const/router";
export default function App() {
  const navigate = useNavigate();
  useEffect(() => {
    navigate("/login");
  }, []);
  return (
    <>
      {/* <HeaderLayout /> */}
      <Routes>
        {publicRouter.map((el, i) => (
          <Route
            key={i}
            path={el.path}
            element={<el.component></el.component>}
          ></Route>
        ))}
      </Routes>
      {/* <FooterLayout /> */}
    </>
  );
}
