const { alias } = require("react-app-rewire-alias");
const { injectBabelPlugin } = require("react-app-rewired");
const rewireLess = require("react-app-rewire-less");

module.exports = function override(config) {
  alias({
    "@core": "src/core",
    "@features": "src/features",
    "@pages": "src/pages",
    "@components": "src/components",
    "@assets": "src/assets",
    "@const": "src/const",
  })(config);
  rewireLess.withLoaderOptions({
    modifyVars: { "@primary-color": "#1DA57A" },
  })(config);
  return config;
};
